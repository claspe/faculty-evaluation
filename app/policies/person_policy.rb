class PersonPolicy < ApplicationPolicy
  attr_reader :user, :person
  def initialize(user, person)
    @user = user
    @person = person
    @department = user.department
    @college = @department.college
  end

  class Scope < Scope
    def resolve
    scope
    end
  end
  def show?
    user.has_role?(:department_admin, @department ) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def index?
    user.has_role?(:department_admin, @department ) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def create?
    user.has_role?(:department_admin, @department ) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def new?
    user.has_role?(:department_admin, @department) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def edit
    user.has_role?(:department_admin, @department) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def update?
    user.has_role?(:department_admin, @department) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def destroy?
    user.has_role?(:department_admin, @department) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def makeadmin?
    user.has_role?(:department_admin, @department) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def revokeadmin?
    user.has_role?(:department_admin, @department) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
end
