class DepartmentPolicy < ApplicationPolicy
  attr_reader :user, :department
  def initialize(user, department)
    @user = user
    @department = department
    @college = @department.college
  end

  class Scope < Scope
    def resolve
    scope
    end
  end
  def show?
    user.has_role?(:department_admin, @department ) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def index?
    user.has_role?(:department_admin, @department ) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def create?
    user.has_role?(:department_admin, @department ) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def new?
    user.has_role?(:department_admin, @department) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def edit
    user.has_role?(:department_admin, @department) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def update?
    user.has_role?(:department_admin, @department) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
  def destroy?
    user.has_role?(:department_admin, @department) or user.has_role?(:global_admin) or user.has_role?(:department_admin, @college)
  end
end
