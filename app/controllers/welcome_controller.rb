class WelcomeController < ApplicationController

  def index
    @person = current_user
  end
end
