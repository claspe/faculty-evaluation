class ChangeReportYearTypeToDate < ActiveRecord::Migration[5.0]
  def up
    change_column :reports, :report_year, 'date using report_year::text::date'
  end
  def down
    change_column :reports, :report_year, 'integer using report_year::text::integer'
  end
end
