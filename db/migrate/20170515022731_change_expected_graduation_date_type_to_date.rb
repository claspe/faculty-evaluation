class ChangeExpectedGraduationDateTypeToDate < ActiveRecord::Migration[5.0]
  def up
     change_column :mentees, :expected_graduation, :date
  end
  def down
     change_column :mentees, :expected_graduation, :datetime
  end
end
